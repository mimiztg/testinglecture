package com.zuehlke.application;

import com.zuehlke.configuration.WorkshopConfiguration;
import com.zuehlke.dao.GradeConfigurationDAO;
import com.zuehlke.dto.grade.GradeConfiguration;
import com.zuehlke.dto.grade.GradeProperty;
import com.zuehlke.dto.grade.GradeValue;
import com.zuehlke.resources.checkgrade.EvaluateGrade;
import com.zuehlke.resources.configuregrading.gradeconfiguration.ConfigureGrading;
import io.dropwizard.Application;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import org.eclipse.jetty.servlets.CrossOriginFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import java.util.EnumSet;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by mimi on 3/13/2016.
 */
public class WorkshopApplication extends Application<WorkshopConfiguration> {

    static final Logger LOGGER = LoggerFactory.getLogger(WorkshopApplication.class);

    private final HibernateBundle<WorkshopConfiguration> hibernateBundle =
            new HibernateBundle<WorkshopConfiguration>(GradeConfiguration.class, GradeProperty.class) {
                @Override
                public DataSourceFactory getDataSourceFactory(WorkshopConfiguration configuration) {
                    return configuration.getDatabase();
                }
            };

    public static void main(String[] args) throws Exception {
        new WorkshopApplication().run(args);
    }

    @Override
    public void run(WorkshopConfiguration workshopConfiguration, Environment environment) throws Exception {
        final GradeConfigurationDAO gradeConfigurationDAO = new GradeConfigurationDAO(hibernateBundle.getSessionFactory());

        final ConfigureGrading configureGrading = new ConfigureGrading(gradeConfigurationDAO);
        final EvaluateGrade evaluateGrade = new EvaluateGrade(gradeConfigurationDAO);

        environment.jersey().register(configureGrading);
        environment.jersey().register(evaluateGrade);

        final FilterRegistration.Dynamic cors =
                environment.servlets().addFilter("CORS", CrossOriginFilter.class);

        // Configure CORS parameters
        cors.setInitParameter("allowedOrigins", "*");
        cors.setInitParameter("allowedHeaders", "X-Requested-With,Content-Type,Accept,Origin");
        cors.setInitParameter("allowedMethods", "OPTIONS,GET,PUT,POST,DELETE,HEAD");

        // Add URL mapping
        cors.addMappingForUrlPatterns(EnumSet.allOf(DispatcherType.class), true, "/*");

    }


    @Override
    public void initialize(Bootstrap<WorkshopConfiguration> bootstrap) {
        bootstrap.addBundle(hibernateBundle);
    }
}
