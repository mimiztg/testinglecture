package com.zuehlke.resources.configuregrading.gradeconfiguration;

import com.zuehlke.dao.GradeConfigurationDAO;
import com.zuehlke.dto.grade.GradeConfiguration;
import com.zuehlke.dto.grade.GradeProperty;
import com.zuehlke.dto.grade.exc.InvalidGradePropertiesArray;
import io.dropwizard.hibernate.UnitOfWork;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import java.util.List;

/**
 * Created by mimi on 3/13/2016.
 */
@Path("configurations")
public class ConfigureGrading {

    private final GradeConfigurationDAO gradeConfigurationDAO;

    public ConfigureGrading(GradeConfigurationDAO gradeConfigurationDAO) {
        this.gradeConfigurationDAO = gradeConfigurationDAO;
    }

    @POST
    @UnitOfWork
    public GradeConfiguration configureGrading(GradeConfiguration gradeConf, @Context final HttpServletResponse response) {

        GradeConfiguration gradeFromDB = gradeConfigurationDAO.getGradeConfiguration();
        if (gradeFromDB == null) {
            return gradeConfigurationDAO.setGradeConfiguration(gradeConf);
        }
        try {
            gradeFromDB.setGradeProperties(gradeConf.getGradeProperties());
        } catch (InvalidGradePropertiesArray invalidGradePropertiesArray) {
            return returnBadRequest(response);
        }
        return gradeConfigurationDAO.setGradeConfiguration(gradeFromDB);
    }

    private GradeConfiguration returnBadRequest(@Context HttpServletResponse response) {
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        try {
            response.flushBuffer();
        } catch (Exception e) {
        } finally {
            return null; //returns empty response with err code  if we enter this block
        }
    }

    @GET
    @UnitOfWork
    public GradeConfiguration getGrading() {
        GradeConfiguration gradeFromDB = gradeConfigurationDAO.getGradeConfiguration();
        return gradeFromDB;
    }

    @DELETE
    @UnitOfWork
    public GradeConfiguration deleteGradeProperty(GradeProperty gradeProperty) {
        GradeConfiguration gradeFromDB = gradeConfigurationDAO.getGradeConfiguration();

        gradeFromDB.removeGradeProperty(gradeProperty);

        return gradeConfigurationDAO.setGradeConfiguration(gradeFromDB);
    }

    @PUT
    @UnitOfWork
    public GradeConfiguration addGradeProperty(GradeProperty gradeProperty, @Context final HttpServletResponse response) {
        GradeConfiguration gradeFromDB = gradeConfigurationDAO.getGradeConfiguration();
        try {
            if (gradeFromDB == null) {
                gradeFromDB = new GradeConfiguration();
                gradeFromDB.addGradeProperty(gradeProperty);
                return gradeConfigurationDAO.setGradeConfiguration(gradeFromDB);
            }
            gradeFromDB.addGradeProperty(gradeProperty);

        } catch (InvalidGradePropertiesArray invalidGradePropertiesArray) {
            return returnBadRequest(response);
        }
        return gradeConfigurationDAO.setGradeConfiguration(gradeFromDB);
    }
}
