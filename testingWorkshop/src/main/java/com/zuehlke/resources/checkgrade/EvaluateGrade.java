package com.zuehlke.resources.checkgrade;

import com.zuehlke.dao.GradeConfigurationDAO;
import com.zuehlke.dto.grade.GradeConfiguration;
import com.zuehlke.dto.grade.GradeProperty;
import com.zuehlke.dto.grade.GradeValue;
import com.zuehlke.dto.grade.exc.DidNotPassException;
import com.zuehlke.dto.grade.exc.InvalidGradeException;
import com.zuehlke.dto.grade.exc.InvalidGradeToPropertyMatch;
import com.zuehlke.dto.grade.exc.InvalidGradeValue;
import io.dropwizard.hibernate.UnitOfWork;

import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Context;
import java.util.List;

/**
 * Created by mimi on 3/29/2016.
 */
@Path("evaluations")
public class EvaluateGrade {

    private final GradeConfigurationDAO gradeConfigurationDAO;

    public EvaluateGrade(GradeConfigurationDAO gradeConfigurationDAO) {
        this.gradeConfigurationDAO = gradeConfigurationDAO;
    }

    @POST
    @UnitOfWork
    public GradeValue calculateFinalGrade(List<GradeValue> grades, @Context final HttpServletResponse response) {
        GradeConfiguration gradeConfigFromDB = gradeConfigurationDAO.getGradeConfiguration();
        GradeValue finalGrade = new GradeValue();
        finalGrade.setName("Final Grade");
        if (gradeConfigFromDB != null) {
            for (GradeValue grade : grades) {
                //find property with name ... if not exists -> err
                GradeProperty prop = gradeConfigFromDB.getGradePropertyByName(grade.getName()).orElse(null);
                try {
                if (grade.isValidComposite()) {
                        finalGrade.addValue(prop.calculateCompositGrade(grade));
                } else {
                    // handle non composite grades
                    if (prop == null || grade.getValue() > 100 || grade.getValue() < 0) {
                        //ilegal states. User sent request containing grading property which is not included in db.
                        return setBadRequestInResponse(response);
                    } else {
                        //grade property exists. we calculate final grade.
                            finalGrade.addValue(prop.scaleToFinalGrade(grade));
                    }
                }
                } catch (InvalidGradeException e) {
                    return setBadRequestInResponse(response);
                } catch (DidNotPassException e) {
                    finalGrade.setFailed();
                    return finalGrade;
                } catch (InvalidGradeToPropertyMatch invalidGradeToPropertyMatch) {
                    return setBadRequestInResponse(response);
                } catch (InvalidGradeValue invalidGradeValue) {
                    return setBadRequestInResponse(response);
                }
            }
        }
        return finalGrade;
    }

    private GradeValue setBadRequestInResponse(@Context HttpServletResponse response) {
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        try {
            response.flushBuffer();
        } catch (Exception e) {
        } finally {
            return null; //returns empty response with err code  if we enter this block
        }
    }

}
