package com.zuehlke.dto.grade;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zuehlke.dto.grade.exc.DidNotPassException;
import com.zuehlke.dto.grade.exc.InvalidGradePropertiesArray;
import com.zuehlke.dto.grade.exc.InvalidGradeToPropertyMatch;
import com.zuehlke.dto.grade.exc.InvalidGradeValue;
import javassist.bytecode.stackmap.BasicBlock;
import org.hibernate.annotations.Cascade;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by mimi on 3/13/2016.
 */

@NamedQueries({
        @NamedQuery(name = "GradeProperty.find", query = "from GradeProperty g"),
})

@Entity
@DiscriminatorValue("gradeProperty")
public class GradeProperty extends ExamPart {

    boolean compulsoryToPass;

    @OneToMany(mappedBy = "gradePropertyId", orphanRemoval = true)
    @JsonIgnore
    @Cascade({org.hibernate.annotations.CascadeType.ALL})
    List<ExamPart> examParts;

    @JsonIgnore
    @ManyToOne
    GradeConfiguration gradeConfiguration;

    public GradeProperty(String name, boolean compulsoryToPass, float passPercentage, float finalGradePercentage) {
        super(name, passPercentage, finalGradePercentage);
        this.compulsoryToPass = compulsoryToPass;
    }

    public GradeConfiguration getGradeConfiguration() {
        return gradeConfiguration;
    }

    public void setGradeConfiguration(GradeConfiguration gradeConf) {
        this.gradeConfiguration = gradeConf;
    }


    //TODO explain pojo test
    public float scaleToFinalGrade(final GradeValue gradeValue) throws DidNotPassException, InvalidGradeToPropertyMatch {
        if (!this.name.equals(gradeValue.getName())) {
            throw new InvalidGradeToPropertyMatch("property name: " + name + " not a mach with grade: " + gradeValue.getName());
        }
        float finalGradePart = gradeValue.getValue() * finalGradePercentage / 100;
        if (compulsoryToPass && gradeValue.getValue() < passPercentage) {
            throw new DidNotPassException("Student has: " + gradeValue.getValue() + " on " + gradeValue.getName() + " and is required to have at least: " + passPercentage + " in order to pass");
        }
        return finalGradePart;
    }


    public List<ExamPart> getExamParts() {
        if (examParts == null) {
            return Collections.EMPTY_LIST;
        }
        return Collections.unmodifiableList(examParts);
    }

    public void setExamParts(List<ExamPart> examParts) {
        this.examParts = examParts;
    }

    public void addExamPart(ExamPart part) throws InvalidGradePropertiesArray {
        if (examParts == null) {
            examParts = new ArrayList<>();
        }
        double sum = examParts.stream().mapToDouble(ExamPart::getFinalGradePercentage).sum();
        if (sum + part.getFinalGradePercentage() > 100) throw new InvalidGradePropertiesArray();
        examParts.add(part);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GradeProperty)) return false;
        if (!super.equals(o)) return false;

        GradeProperty that = (GradeProperty) o;

        if (compulsoryToPass != that.compulsoryToPass) return false;
        if (examParts != null ? !examParts.equals(that.examParts) : that.examParts != null) return false;
        return gradeConfiguration != null ? gradeConfiguration.equals(that.gradeConfiguration) : that.gradeConfiguration == null;

    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + (compulsoryToPass ? 1 : 0);
        result = 31 * result + (gradeConfiguration != null ? gradeConfiguration.hashCode() : 0);
        return result;
    }

    public float calculateCompositGrade(GradeValue grade) throws InvalidGradeValue, DidNotPassException {
        if (this.examParts == null || this.getExamParts().isEmpty() || this.getExamParts().size() != grade.getCompositValues().size()) {
            throw new InvalidGradeValue();
        }
        if (!grade.isValidComposite()) {
            throw new InvalidGradeValue();
        }
        // if both are valid match -> calculate grade
        float totalUnscaledGrade = 0;

        for (GradeValue gr : grade.getCompositValues()) {
            try {
                for (ExamPart part : examParts) {
                    if (gr.getName() == part.getName()) {
                        //matched
                        if (gr.getValue() < part.getPassPercentage())
                            throw new DidNotPassException("criteria not met with " + gr.getName());
                        totalUnscaledGrade = totalUnscaledGrade + gr.getValue() * part.getFinalGradePercentage() / 100;
                    }
                }
            }catch(DidNotPassException e){
               // nothing to add. student did not pass one part of a single exam not an entire exam :)
            }
        }
        float scaledGrade = totalUnscaledGrade * this.getFinalGradePercentage() / 100;
        if (this.compulsoryToPass && scaledGrade < this.getPassPercentage()) throw new DidNotPassException("criteria not met with " + this.getName());
        return scaledGrade;
    }

}
