package com.zuehlke.dto.grade;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

import javax.persistence.*;

/**
 * Created by mimi on 8/28/2016.
 */
@Entity
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "DTYPE", discriminatorType = DiscriminatorType.STRING)
@DiscriminatorValue(value = "gradePropertyId")
@JsonTypeInfo(
        use = JsonTypeInfo.Id.NAME,
        include = JsonTypeInfo.As.PROPERTY,
        property = "type",
        defaultImpl = GradeProperty.class)
@JsonSubTypes({
        @JsonSubTypes.Type(value = GradeProperty.class, name = "gradeProperty")})

public class ExamPart {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long gradePropertyId;

    @Column(unique = true)
    String name;

    float passPercentage;
    float finalGradePercentage;
    private ExamPart() {
    }

    public float getPassPercentage() {
        return passPercentage;
    }

    public String getName() {
        return name;
    }

    public float getFinalGradePercentage() {
        return finalGradePercentage;
    }

    public ExamPart(String name, float passPercentage, float finalGradePercentage) {
        this.name = name;
        this.passPercentage = passPercentage;
        this.finalGradePercentage = finalGradePercentage;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPassPercentage(float passPercentage) {
        this.passPercentage = passPercentage;
    }

    public void setFinalGradePercentage(float finalGradePercentage) {
        this.finalGradePercentage = finalGradePercentage;
    }

    public Long getGradePropertyId() {
        return gradePropertyId;
    }

    public void setGradePropertyId(Long gradePropertyId) {
        this.gradePropertyId = gradePropertyId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof ExamPart)) return false;

        ExamPart examPart1 = (ExamPart) o;

        if (Float.compare(examPart1.passPercentage, passPercentage) != 0) return false;
        if (Float.compare(examPart1.finalGradePercentage, finalGradePercentage) != 0) return false;
        if (!this.gradePropertyId.equals(examPart1.gradePropertyId)) return false;
        return name.equals(examPart1.name);

    }

    @Override
    public int hashCode() {
        int result = gradePropertyId.hashCode();
        result = 31 * result + name.hashCode();
        result = 31 * result + (passPercentage != +0.0f ? Float.floatToIntBits(passPercentage) : 0);
        result = 31 * result + (finalGradePercentage != +0.0f ? Float.floatToIntBits(finalGradePercentage) : 0);
        return result;
    }
}
