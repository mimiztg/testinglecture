package com.zuehlke.dto.grade;

import com.zuehlke.dto.grade.exc.InvalidGradeException;

import javax.persistence.Entity;
import java.util.Collections;
import java.util.List;

/**
 * Created by mimi on 3/13/2016.
 */
public class GradeValue {

    private String name;

    //points, expected 0..100
    private float value;

    private List<GradeValue> compositValues;

    public GradeValue(String name, List<GradeValue> compositValues) {
        this.name = name;
        this.compositValues = compositValues;
    }

    public float getValue() {
        return value;
    }

    public boolean isValidComposite() {
        return compositValues != null && !compositValues.isEmpty() && compositValues.stream().noneMatch(gradeValue -> {
            return (gradeValue.getValue() > 100) || (gradeValue.getValue() < 0);
        });

    }

    public GradeValue() {
    }

    public GradeValue(String name, float value) {
        this.name = name;
        this.value = value;
    }

    public void setValue(float value) throws InvalidGradeException {
        if (this.value + value > 100) {
            throw new InvalidGradeException("Grade value cannot exceed 100");
        }
        this.value = value;
    }

    public void setFailed() {
        this.value = 0;
    }

    public void addValue(float value) throws InvalidGradeException {
        if (this.value + value > 100) {
            throw new InvalidGradeException("Grade value cannot exceed 100");
        }
        this.value += value;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<GradeValue> getCompositValues() {
        return Collections.unmodifiableList(compositValues);
    }

    public void setCompositValues(List<GradeValue> compositValues) {
        this.compositValues = compositValues;
    }
}
