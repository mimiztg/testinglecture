package com.zuehlke.dto.grade.exc;

/**
 * Created by mimi on 3/29/2016.
 */
public class InvalidGradeToPropertyMatch extends Exception {
    public InvalidGradeToPropertyMatch(String s) {
        super(s);
    }
}
