package com.zuehlke.dto.grade.exc;

/**
 * Created by mimi on 3/29/2016.
 */
public class InvalidGradeException extends Exception {
    public InvalidGradeException(String s) {
        super(s);
    }
}
