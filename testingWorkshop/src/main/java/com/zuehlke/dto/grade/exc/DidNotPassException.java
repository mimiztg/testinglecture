package com.zuehlke.dto.grade.exc;

/**
 * Created by mimi on 3/29/2016.
 */
public class DidNotPassException extends Exception {
    public DidNotPassException(String s) {
        super(s);
    }
}
