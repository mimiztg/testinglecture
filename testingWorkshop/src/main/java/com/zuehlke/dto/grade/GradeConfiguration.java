package com.zuehlke.dto.grade;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.zuehlke.dto.grade.exc.InvalidGradePropertiesArray;
import org.assertj.core.util.Lists;
import org.hibernate.annotations.*;

import javax.persistence.*;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Created by mimi on 3/13/2016.
 */


@NamedQueries({
        @NamedQuery(name = "GradeConfiguration.find", query = "from GradeConfiguration g"),
})


@Entity
public class GradeConfiguration {

    @Id
    @JsonIgnore
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long gradeConfigurationId;


    @OneToMany
    @Cascade(org.hibernate.annotations.CascadeType.ALL)
    List <GradeProperty> gradeProperties;

    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval=true)
    public List<GradeProperty> getGradeProperties() {
        return gradeProperties;
    }

    public void setGradeProperties(List<GradeProperty> gradeProperties) throws InvalidGradePropertiesArray {
        double total = gradeProperties.stream().mapToDouble(GradeProperty::getFinalGradePercentage).sum();
        if (total > 100 || total < 0) {
            throw new InvalidGradePropertiesArray();
        }
        this.gradeProperties = gradeProperties;
    }

    public Optional<GradeProperty> getGradePropertyByName (String uniqueName) {
        return gradeProperties.stream().filter(prop -> prop.getName().equals(uniqueName)).findFirst();
    }

    public void removeGradeProperty(GradeProperty gradeProperty) {
        gradeProperties.removeIf(gradeProp -> gradeProp.getName().equalsIgnoreCase(gradeProperty.getName()));
    }

    public void addGradeProperty(GradeProperty gradeProperty) throws InvalidGradePropertiesArray {
        if (gradeProperties == null) {
            gradeProperties = new ArrayList<>();
        }
        double total = gradeProperties.stream().mapToDouble(GradeProperty::getFinalGradePercentage).sum();
        if (total + gradeProperty.getFinalGradePercentage() > 100 || gradeProperty.getName() == null) {
            throw new InvalidGradePropertiesArray();
        }
        boolean percentagesMathch = true;
        if (gradeProperties.size() > 0) {
            percentagesMathch = gradeProperties.stream().anyMatch(gradeProp -> {
                        if (gradeProp.getExamParts() != null && !gradeProp.getExamParts().isEmpty()) {
                            Double examPercentageTotal = gradeProp.getExamParts().stream().mapToDouble(ExamPart::getFinalGradePercentage).sum();
                            if (examPercentageTotal > 100) return false;
                        }
                        return true;
                    }
            );
        }
        if (!percentagesMathch) {throw new InvalidGradePropertiesArray();}
        this.gradeProperties.add(gradeProperty);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GradeConfiguration)) return false;

        GradeConfiguration that = (GradeConfiguration) o;

        if (gradeConfigurationId != null ? !gradeConfigurationId.equals(that.gradeConfigurationId) : that.gradeConfigurationId != null)
            return false;
        return gradeProperties != null ? gradeProperties.equals(that.gradeProperties) : that.gradeProperties == null;

    }

    @Override
    public int hashCode() {
        int result = gradeConfigurationId != null ? gradeConfigurationId.hashCode() : 0;
        result = 31 * result + (gradeProperties != null ? gradeProperties.hashCode() : 0);
        return result;
    }
}
