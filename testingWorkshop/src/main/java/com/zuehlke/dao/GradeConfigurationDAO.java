package com.zuehlke.dao;

import com.zuehlke.dto.grade.GradeConfiguration;
import com.zuehlke.dto.grade.GradeProperty;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;

/**
 * Created by mimi on 3/13/2016.
 */
public class GradeConfigurationDAO extends AbstractDAO<GradeConfiguration> {

    public GradeConfigurationDAO(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    public GradeConfiguration getGradeConfiguration() {
        List<GradeConfiguration> gradeConfigurationList = list(namedQuery("GradeConfiguration.find"));
        if (gradeConfigurationList == null || gradeConfigurationList.isEmpty() ){
            return null;
        }
        return gradeConfigurationList.get(0);
    }

    public GradeConfiguration setGradeConfiguration(GradeConfiguration updatedGradeConfiguration){
        return persist(updatedGradeConfiguration);

    }

}
