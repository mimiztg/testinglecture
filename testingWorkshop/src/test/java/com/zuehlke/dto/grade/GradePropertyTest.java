package com.zuehlke.dto.grade;

import com.zuehlke.dto.grade.exc.DidNotPassException;
import com.zuehlke.dto.grade.exc.InvalidGradeException;
import com.zuehlke.dto.grade.exc.InvalidGradePropertiesArray;
import com.zuehlke.dto.grade.exc.InvalidGradeToPropertyMatch;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotEquals;

/**
 * Created by mimi on 3/30/2016.
 */
public class GradePropertyTest {


    public static final GradeProperty FIRST = new GradeProperty("first", false, 0, 30);
    public static final GradeProperty SECOND = new GradeProperty("second", true, 50, 30);
    public static final GradeProperty THIRD = new GradeProperty("third", false, 50, 30);
    protected GradeConfiguration gradeConfiguration;

    @Before
    public void setUp() {

    }

    @Test(expected = InvalidGradeToPropertyMatch.class)
    public void testScaleToFinalGrade_WithInvalidGradeProperty() throws DidNotPassException, InvalidGradeToPropertyMatch {
        GradeValue value = new GradeValue();
        try {
            value.setName("not-valid");
            value.setValue(10);
        } catch (InvalidGradeException e) {
            //case tested in GradeValueTest
        }
        FIRST.scaleToFinalGrade(value);

    }

    @Test(expected = DidNotPassException.class)
    public void testScaleToFinalGrade_StudentDidNotPass() throws DidNotPassException, InvalidGradeToPropertyMatch {
        GradeValue value = new GradeValue();
        try {
            value.setName("second");
            value.setValue(10);
        } catch (InvalidGradeException e) {
            //case tested in GradeValueTest
        }
        SECOND.scaleToFinalGrade(value);
    }

    @Test
    public void testScaleToFinalGrade_withValidGradePropertyName() throws DidNotPassException, InvalidGradeToPropertyMatch {
        GradeValue value = new GradeValue();
        try {
            value.setName("third");
            value.setValue(100);
        } catch (InvalidGradeException e) {
            //case tested in GradeValueTest
        }
        assertEquals(30, THIRD.scaleToFinalGrade(value), 0);
    }

    @Test
    public void whenAddingTwoGrades_thenExpectTwoGrades() throws InvalidGradePropertiesArray {
        //creating composit grade
        GradeProperty compositGrade = generateCompositGradeWithTwoSubExams();

        List<ExamPart> examParts = compositGrade.getExamParts();
        assertEquals(examParts.size(), 2);
    }

    @Test
    public void whenAddingTwoGradesWithSpecificValues_thenExpectTwoGradesWithsSpecificValues() throws InvalidGradePropertiesArray {
        //creating composit grade
        GradeProperty compositGrade = generateCompositGradeWithTwoSubExams();

        List<ExamPart> examParts = compositGrade.getExamParts();
        assertEquals(examParts.get(0).getName(), "Usmeni");
        assertEquals(examParts.get(0).getPassPercentage(), 0, 0);
        assertEquals(examParts.get(0).getFinalGradePercentage(), 50, 0);

        assertEquals(examParts.get(1).getName(), "Pismeni");
        assertEquals(examParts.get(1).getPassPercentage(), 51, 0);
        assertEquals(examParts.get(1).getFinalGradePercentage(), 50, 0);
    }

    @Test
    public void whenComparingTwoEqualCompositGradeProperties_thenResultShouldBeTrue() throws InvalidGradePropertiesArray {
        GradeProperty compositGrade1 = new GradeProperty("composit", false, 0, 30);
        compositGrade1.setGradePropertyId((long) 1);    //will be set by Hibernate
        ExamPart usmeni = new ExamPart("Usmeni", 0, 50);
        usmeni.setGradePropertyId((long) 2);
        compositGrade1.addExamPart(usmeni);

        GradeProperty compositGrade2 = new GradeProperty("composit", false, 0, 30);
        compositGrade2.setGradePropertyId((long) 1);    //will be set by Hibernate
        ExamPart usmeni2 = new ExamPart("Usmeni", 0, 50);
        usmeni2.setGradePropertyId((long) 2);
        compositGrade2.addExamPart(usmeni);

        assertEquals(compositGrade1, compositGrade2);
    }

    @Test
    public void whenComparingTwoNonEqualCompositGradeProperties_thenResultShouldBefalse() throws InvalidGradePropertiesArray {
        GradeProperty compositGrade1 = new GradeProperty("composit", false, 0, 30);
        compositGrade1.setGradePropertyId((long) 1);    //will be set by Hibernate
        ExamPart usmeni = new ExamPart("Usmeni", 20, 50);
        usmeni.setGradePropertyId((long) 2);
        compositGrade1.addExamPart(usmeni);

        GradeProperty compositGrade2 = new GradeProperty("composit", false, 0, 30);
        compositGrade2.setGradePropertyId((long) 1);    //will be set by Hibernate
        ExamPart usmeni2 = new ExamPart("Usmeni", 33, 50);
        usmeni2.setGradePropertyId((long) 2);
        compositGrade2.addExamPart(usmeni2);

        assertNotEquals(compositGrade1, compositGrade2);
    }

    private GradeProperty generateCompositGradeWithTwoSubExams() throws InvalidGradePropertiesArray {
        GradeProperty compositGrade = new GradeProperty("composit", false, 0, 30);
        ExamPart usmeni = new ExamPart("Usmeni", 0, 50);
        compositGrade.addExamPart(usmeni);
        ExamPart pismeni = new ExamPart("Pismeni", 51, 50);
        compositGrade.addExamPart(pismeni);
        return compositGrade;
    }
}
