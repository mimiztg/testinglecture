package com.zuehlke.dto.grade;

import com.zuehlke.dto.grade.exc.InvalidGradePropertiesArray;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;

/**
 * Created by mimi on 3/30/2016.
 */
public class GradeConfigurationTest {

    public static final GradeProperty FIRST = new GradeProperty("first", false, 0, 30);
    public static final GradeProperty SECOND = new GradeProperty("second", false, 0, 30);
    public static final GradeProperty THIRD = new GradeProperty("third", false, 0, 40);
    public static final GradeProperty FOURTH = new GradeProperty("fourth", false, 0, 10);
    public static final GradeProperty COMPOSITE = new GradeProperty("composite", false, 0, 10);
    static final ExamPart PART_1 = new ExamPart("deo ispita 1", 0, 50);
    static final ExamPart PART_2 = new ExamPart("deo ispita 2", 0, 40);
    static final ExamPart PART_3 = new ExamPart("deo ispita 3", 0, 80);
    protected GradeConfiguration gradeConfiguration;

    @Before
    public void setUp()  {
        gradeConfiguration = new GradeConfiguration();
    }

    @Test
    public void testGettingGradePropertyByName() throws Exception {
        setupGradeProperty();
        assertEquals(gradeConfiguration.getGradePropertyByName("second").get(), SECOND);
        assertEquals(gradeConfiguration.getGradePropertyByName("third").get(), THIRD);
    }

    @Test
    public void testRemovingGradeProperty() throws Exception {
        setupGradeProperty();
        assertEquals(gradeConfiguration.getGradePropertyByName("second").get(), SECOND);
        gradeConfiguration.removeGradeProperty(GradePropertyTest.SECOND);
        assertEquals(gradeConfiguration.getGradePropertyByName("second").orElse(null),null);
        assertEquals(gradeConfiguration.getGradePropertyByName("third").get(), THIRD);
    }

    @Test(expected = InvalidGradePropertiesArray.class)
    public void testSetGradePropertiesListOwerflow() throws Exception, InvalidGradePropertiesArray {
        setupGradeProperty();
        List<GradeProperty> gradeProperties = gradeConfiguration.getGradeProperties();
        gradeProperties.add(new GradeProperty("too much", false, 0, 20));
        gradeConfiguration.setGradeProperties(gradeProperties);
    }

    @Test(expected = InvalidGradePropertiesArray.class)
    public void testAddingGradePropertyOverflow() throws Exception, InvalidGradePropertiesArray {
        setupGradeProperty();
        gradeConfiguration.addGradeProperty(FOURTH);
    }

    @Test
    public void testAddingNewPropertyHappyPath() throws InvalidGradePropertiesArray {
        GradeConfiguration gradeConfiguration = new GradeConfiguration();
        GradeProperty gradeProperty = new GradeProperty("Kolokvijum 1", false, 50, 50);
        assertEquals(null, gradeConfiguration.getGradeProperties());
        gradeConfiguration.addGradeProperty(gradeProperty);

        assertEquals(gradeProperty,gradeConfiguration.getGradePropertyByName("Kolokvijum 1").get());
        assertEquals(1, gradeConfiguration.getGradeProperties().size());
    }

    @Test(expected = InvalidGradePropertiesArray.class)
    public void whenAddingGradePropertyWithInvalidExamParts_thenExceptionShouldBeThrown() throws Exception, InvalidGradePropertiesArray {
        setupIncompleteGradeProperty();
        COMPOSITE.addExamPart(PART_1);
        COMPOSITE.addExamPart(PART_2);
        COMPOSITE.addExamPart(PART_3);
        gradeConfiguration.addGradeProperty(COMPOSITE);
    }


    private void setupGradeProperty() {
        List<GradeProperty> gradePropertyList = new ArrayList();
        gradePropertyList.add(FIRST);
        gradePropertyList.add(SECOND);
        gradePropertyList.add(THIRD);

        try {
            gradeConfiguration.setGradeProperties(gradePropertyList);
        } catch (InvalidGradePropertiesArray invalidGradePropertiesArray) {
            invalidGradePropertiesArray.printStackTrace();
        }
    }
    private void setupIncompleteGradeProperty() {
        List<GradeProperty> gradePropertyList = new ArrayList();
        gradePropertyList.add(FIRST);
        gradePropertyList.add(SECOND);
        try {
            gradeConfiguration.setGradeProperties(gradePropertyList);
        } catch (InvalidGradePropertiesArray invalidGradePropertiesArray) {
            invalidGradePropertiesArray.printStackTrace();
        }
    }
}