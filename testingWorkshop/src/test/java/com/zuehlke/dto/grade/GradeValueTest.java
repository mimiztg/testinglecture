package com.zuehlke.dto.grade;

import com.zuehlke.dto.grade.exc.InvalidGradeException;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by mimi on 3/30/2016.
 */
public class GradeValueTest {

    GradeValue gradeValue;
    Exception ex;

    @Before
    public void setUp() throws Exception {
        gradeValue = new GradeValue();
    }

    @Test(expected = InvalidGradeException.class)
    public void testSetValueAndExpectOverflowException() throws Exception {
        gradeValue.setValue(102);
    }

    @Test
    public void testSetValueAndExpectValueIsSet() throws Exception {

        try {
            gradeValue.setValue(100);
        } catch (Exception e) {
            ex = e;
        }
        assertEquals(null, ex);
    }

    @Test(expected = InvalidGradeException.class)
    public void testAddValueAndExpectOverflowException() throws Exception {
        gradeValue.setValue(90);
        gradeValue.addValue(11);
    }

    @Test
    public void testAddValueEdgeCase() throws Exception {
        try {
            gradeValue.setValue(90);
            gradeValue.addValue(10);
        } catch (Exception e) {
            ex = e;
        }
        assertEquals(null, ex);
    }

    @Test
    public void testAddValueHappyPath() throws Exception {
        try {
            gradeValue.setValue(50);
            gradeValue.addValue(10);
        } catch (Exception e) {
            ex = e;
        }
        assertEquals(null, ex);
    }
}