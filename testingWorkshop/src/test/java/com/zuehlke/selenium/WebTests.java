package com.zuehlke.selenium;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.scenarioo.api.ScenarioDocuWriter;

import java.io.File;

import static org.junit.Assert.assertTrue;

/**
 * Created by mimi on 4/4/2016.
 */
public class WebTests {
    private ScenarioDocuWriter writer = new ScenarioDocuWriter(new File("data"), "workshopApplication", "startingBuild");

    @Rule
    public ScenariooRule scenariooRule = new ScenariooRule(writer);

    private static final String BASE_URL = "http://localhost:9000/#/";
    private WebDriver webDriver;

    @Before
    public void createDriver() {
        EventFiringWebDriver eventFiringWebDriver = new EventFiringWebDriver(new FirefoxDriver());
        eventFiringWebDriver.register(new ScenariooSeleniumListener(writer, scenariooRule));
        webDriver = eventFiringWebDriver;
    }

    @After
    public void closeDriver() {
        webDriver.quit();
        writer.flush();
    }

    @Test
    public void openStartingPageAndCheckNavigation() {
        openStartpage();
        assertStartingPage();
        clickOnConfigurationNavButton();
        assertConfigurationPage();
        clickOnEvaluationNavButton();
        assertEvaluationPage();
    }

    @Test
    public void fullSystemIntegrationTest() {
        openStartpage();
        assertStartingPage();
        //add element to configuration
        clickOnConfigurationNavButton();
        assertConfigurationPage();

        //add first examination
        typeValueIntoFieldWithId("nameOfExam", "Semestral1");
        typeValueIntoFieldWithId("finalExamPercentage", "20");
        clickOnElement("addExam");
        assertConfigurationRow(0, "Semestral1", "20", false);

        //add second examination
        typeValueIntoFieldWithId("nameOfExam", "Semestral2");
        typeValueIntoFieldWithId("finalExamPercentage", "30");
        clickOnElement("compulsoryExam");
        typeValueIntoFieldWithId("compulsoryPercentage", "51");
        clickOnElement("addExam");
        assertConfigurationRow(1, "Semestral2", "30", true);

        //try adding exam with more % than allowed
        typeValueIntoFieldWithId("nameOfExam", "Final");
        typeValueIntoFieldWithId("finalExamPercentage", "51");
        clickOnElement("compulsoryExam");
        typeValueIntoFieldWithId("compulsoryPercentage", "");
        try{
            clickOnElement("addExam");
            assertTrue("Test fails! no Modal present!", false);
        } catch (Exception e) {
            //modal appeared, exception thrown.
        }

        if (isAlertPresent()) {
            webDriver.switchTo().alert();
            webDriver.switchTo().alert().accept();
            webDriver.switchTo().defaultContent();
        }

        typeValueIntoFieldWithId("finalExamPercentage", "50");
        clickOnElement("addExam");
        assertConfigurationRow(2, "Final", "50", false);

        clickOnEvaluationNavButton();
        assertEvaluationPage();

        typeValueIntoFieldWithId("grade_0", "90");
        typeValueIntoFieldWithId("grade_1", "40");
        typeValueIntoFieldWithId("grade_2", "90");
        clickOnElement("calculateGrade");
        assertFailed();

        typeValueIntoFieldWithId("grade_1", "50");
        clickOnElement("calculateGrade");
        assertFailed();

        try{
            typeValueIntoFieldWithId("grade_0", "120");
            clickOnElement("calculateGrade");
            assertTrue("Test fails! no Modal present!", false);
        } catch (Exception e) {
            //modal appeared, exception thrown.
        }

        if (isAlertPresent()) {
            webDriver.switchTo().alert();
            webDriver.switchTo().alert().accept();
            webDriver.switchTo().defaultContent();
        }

        typeValueIntoFieldWithId("grade_0", "90");
        typeValueIntoFieldWithId("grade_1", "51");
        clickOnElement("calculateGrade");
        assertPassed();


    }

    public boolean isAlertPresent() {
        try {
            webDriver.switchTo().alert();
            return true;
        } // try
        catch (Exception e) {
            return false;
        } // catch
    }

    private void assertFailed() {

        WebElement failed = webDriver.findElement(By.id("failed"));
        assertTrue(failed.getText().contains("Student has failed the exam!"));
        assertTrue(failed.isDisplayed());
    }

    private void assertPassed() {
        WebElement passed = webDriver.findElement(By.id("passed"));
        assertTrue(passed.getText().contains("Student has passed the exam"));
        assertTrue(passed.isDisplayed());
    }

    private void assertConfigurationRow(int i, String name, String percentage, boolean compulsory) {

        WebDriverWait wait = new WebDriverWait(webDriver, 1);
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("gradeName_" + i)));

        //assert name
        WebElement elementName = webDriver.findElement(By.id("gradeName_" + i));
        assertTrue(elementName.getText().contains(name));

        //assert compulsory
        if (compulsory) {
            WebElement elementCompulsory = webDriver.findElement(By.id("gradeCompulsory_" + i));
            assertTrue(compulsory == (elementCompulsory != null));
        } else {
            WebElement elementNotCompulsory = webDriver.findElement(By.id("gradeNotCompulsory_" + i));
            assertTrue(!compulsory == (elementNotCompulsory != null));
        }
    }


    private void clickOnConfigurationNavButton() {
        getConfigurationNavButton(By.id("configuration")).click();
    }

    private void clickOnEvaluationNavButton() {
        getConfigurationNavButton(By.id("evaluation")).click();
    }

    private WebElement getConfigurationNavButton(By configuration) {
        return webDriver.findElement(configuration);
    }

    private void openStartpage() {
        webDriver.get(BASE_URL);
    }

    private void assertStartingPage() {
        WebElement userName = webDriver.findElement(By.id("testHeading"));
        assertTrue(userName.getText().contains("Welcome to test app"));
    }

    private void assertConfigurationPage() {
        WebElement userName = webDriver.findElement(By.id("testHeadingConfiguration"));
        assertTrue(userName.getText().contains("Configurator"));
    }

    private void assertEvaluationPage() {
        WebElement userName = webDriver.findElement(By.id("testHeadingEvaluation"));
        assertTrue(userName.getText().contains("Evaluator"));
    }

    private void typeValueIntoFieldWithId(String fieldId, String value) {
        WebElement nameTextElement = webDriver.findElement(By.id(fieldId));
        nameTextElement.clear();
        nameTextElement.sendKeys(value);
    }

    private void clickOnElement(String elementId) {
        WebElement elem = webDriver.findElement(By.id(elementId));
        elem.click();
    }

}
