package com.zuehlke;

import com.zuehlke.dto.grade.GradeConfigurationTest;
import com.zuehlke.dto.grade.GradePropertyTest;
import com.zuehlke.dto.grade.GradeValueTest;
import com.zuehlke.resources.checkgrade.EvaluateGradeTest;
import com.zuehlke.resources.configuregrading.gradeconfiguration.ConfigureGradingTest;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;
@RunWith(Suite.class)
@Suite.SuiteClasses({
        GradeConfigurationTest.class,
        GradePropertyTest.class,
        GradeValueTest.class,
        EvaluateGradeTest.class,
        ConfigureGradingTest.class

})
public class JUnitTestSuite {
}  