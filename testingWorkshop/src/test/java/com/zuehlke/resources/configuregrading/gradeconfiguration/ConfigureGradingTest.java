package com.zuehlke.resources.configuregrading.gradeconfiguration;

import com.zuehlke.dao.GradeConfigurationDAO;
import com.zuehlke.dto.grade.GradeConfiguration;
import com.zuehlke.dto.grade.GradeProperty;
import com.zuehlke.dto.grade.exc.InvalidGradePropertiesArray;
import com.zuehlke.resources.GradingTestInfrastructure;
import org.easymock.EasyMock;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;

import static org.easymock.EasyMock.expect;
import static org.easymock.EasyMock.mock;
import static org.junit.Assert.assertEquals;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by mimi on 3/30/2016.
 */
public class ConfigureGradingTest extends GradingTestInfrastructure {

    @Test
    public void testConfigureGrading_notInDb_Mockito() throws Exception, InvalidGradePropertiesArray {
        GradeConfigurationDAO gradeConfigurationDAO = Mockito.mock(GradeConfigurationDAO.class);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

        GradeConfiguration gradeConfiguration = setupGradeConfiguration();

        when(gradeConfigurationDAO.getGradeConfiguration()).thenReturn(null);
        when(gradeConfigurationDAO.setGradeConfiguration(gradeConfiguration)).thenReturn(gradeConfiguration);

        ConfigureGrading configureGrading = new ConfigureGrading(gradeConfigurationDAO);
        assertEquals(gradeConfiguration, configureGrading.configureGrading(gradeConfiguration, response));
    }

    @Test
    public void testConfigureGrading_notInDb_EasyMock() throws Exception, InvalidGradePropertiesArray {
        GradeConfigurationDAO gradeConfigurationDAO = mock(GradeConfigurationDAO.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        GradeConfiguration gradeConfiguration = setupGradeConfiguration();
        expect(gradeConfigurationDAO.getGradeConfiguration()).andReturn(null);
        expect(gradeConfigurationDAO.setGradeConfiguration(gradeConfiguration)).andReturn(gradeConfiguration);
        EasyMock.replay(gradeConfigurationDAO,response);
        ConfigureGrading configureGrading = new ConfigureGrading(gradeConfigurationDAO);
        assertEquals(gradeConfiguration, configureGrading.configureGrading(gradeConfiguration, response));
        EasyMock.verify(gradeConfigurationDAO,response);
    }

    @Test
    public void testConfigureGrading_objInDb_Mockito() throws Exception, InvalidGradePropertiesArray {

        GradeConfiguration gradeConfiguration = setupGradeConfiguration();
        GradeConfiguration newGradeConfiguration = new GradeConfiguration();
        newGradeConfiguration.setGradeProperties(new ArrayList<>());

        GradeConfigurationDAO gradeConfigurationDAO = Mockito.mock(GradeConfigurationDAO.class);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        when(gradeConfigurationDAO.getGradeConfiguration()).thenReturn(gradeConfiguration);
        when(gradeConfigurationDAO.setGradeConfiguration(newGradeConfiguration)).thenReturn(newGradeConfiguration);

        ConfigureGrading configureGrading = new ConfigureGrading(gradeConfigurationDAO);

        assertEquals(newGradeConfiguration, configureGrading.configureGrading(newGradeConfiguration, response));
    }


    @Test
    public void testConfigureGrading_objInDb_EasyMock() throws Exception, InvalidGradePropertiesArray {
        GradeConfigurationDAO gradeConfigurationDAO = mock(GradeConfigurationDAO.class);
        HttpServletResponse response = mock(HttpServletResponse.class);

        GradeConfiguration gradeConfiguration = setupGradeConfiguration();
        GradeConfiguration newGradeConfiguration = new GradeConfiguration();
        newGradeConfiguration.setGradeProperties(new ArrayList<>());
        expect(gradeConfigurationDAO.getGradeConfiguration()).andReturn(gradeConfiguration);
        expect(gradeConfigurationDAO.setGradeConfiguration(newGradeConfiguration)).andReturn(newGradeConfiguration);
        EasyMock.replay(gradeConfigurationDAO,response);
        ConfigureGrading configureGrading = new ConfigureGrading(gradeConfigurationDAO);

        assertEquals(newGradeConfiguration, configureGrading.configureGrading(newGradeConfiguration, response));
        EasyMock.verify(gradeConfigurationDAO,response);
    }


    @Test
    public void testAddGradeProperty_whenConfigIsNull_Mockito() throws Exception, InvalidGradePropertiesArray {
        //test for configureGrading.addGradeProperty
        GradeConfigurationDAO gradeConfigurationDAO = Mockito.mock(GradeConfigurationDAO.class);
        HttpServletResponse httpServletResponse = Mockito.mock(HttpServletResponse.class);
        GradeProperty gradeProperty = new GradeProperty("kolokvijum 1", false, 30, 30);
        gradeProperty.setGradePropertyId((long) 1);
        GradeConfiguration gradeConfigurationToDb = new GradeConfiguration();
        gradeConfigurationToDb.addGradeProperty(gradeProperty);


        when(gradeConfigurationDAO.getGradeConfiguration()).thenReturn(null);
        when(gradeConfigurationDAO.setGradeConfiguration(any())).thenReturn(gradeConfigurationToDb);

        ConfigureGrading configureGrading = new ConfigureGrading(gradeConfigurationDAO);



        GradeConfiguration expectedConfig = new GradeConfiguration();

        GradeProperty expected = new GradeProperty("kolokvijum 1", false, 30, 30);
        expected.setGradePropertyId((long) 1);
        expectedConfig.addGradeProperty(expected);


        assertEquals(expectedConfig,configureGrading.addGradeProperty(gradeProperty,httpServletResponse ));
        verify(gradeConfigurationDAO, times(1)).getGradeConfiguration();
    }


    @Test
    public void testAddGradeProperty_whenConfigIsPresent_EasyMock() throws Exception, InvalidGradePropertiesArray {
        GradeConfigurationDAO gradeConfigurationDAO = mock(GradeConfigurationDAO.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        GradeConfiguration gradeConfiguration = setupGradeConfiguration();
        expect(gradeConfigurationDAO.getGradeConfiguration()).andReturn(gradeConfiguration).times(1);
        expect(gradeConfigurationDAO.setGradeConfiguration(EasyMock.anyObject(GradeConfiguration.class))).andReturn(gradeConfiguration).anyTimes();
        EasyMock.replay(gradeConfigurationDAO,response);
        GradeProperty FOURTH = new GradeProperty("fourth", false, 50, 0);

        ConfigureGrading configureGrading = new ConfigureGrading(gradeConfigurationDAO);
        configureGrading.addGradeProperty(FOURTH, response);
        EasyMock.verify(gradeConfigurationDAO,response);
    }

    @Test
    public void testAddGradeProperty_whenConfigIsNull_EasyMock() throws Exception, InvalidGradePropertiesArray {
        GradeConfigurationDAO gradeConfigurationDAO = mock(GradeConfigurationDAO.class);
        HttpServletResponse response = mock(HttpServletResponse.class);
        GradeConfiguration gradeConfiguration = setupGradeConfiguration();
        expect(gradeConfigurationDAO.getGradeConfiguration()).andReturn(null).times(1);
        expect(gradeConfigurationDAO.setGradeConfiguration(EasyMock.anyObject(GradeConfiguration.class))).andReturn(gradeConfiguration).anyTimes();
        EasyMock.replay(gradeConfigurationDAO,response);
        GradeProperty FOURTH = new GradeProperty("fourth", false, 50, 0);
        ConfigureGrading configureGrading = new ConfigureGrading(gradeConfigurationDAO);
        configureGrading.addGradeProperty(FOURTH, response);
        EasyMock.verify(gradeConfigurationDAO,response);
    }

}