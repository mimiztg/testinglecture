package com.zuehlke.resources.checkgrade;

import com.zuehlke.dao.GradeConfigurationDAO;
import com.zuehlke.dto.grade.GradeConfiguration;
import com.zuehlke.dto.grade.GradeProperty;
import com.zuehlke.dto.grade.GradeValue;
import com.zuehlke.dto.grade.exc.InvalidGradePropertiesArray;
import com.zuehlke.resources.GradingTestInfrastructure;
import org.junit.Test;
import org.mockito.Mockito;

import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

/**
 * Created by mimi on 3/30/2016.
 */
public class EvaluateGradeTest extends GradingTestInfrastructure {

    @Test
    public void testCalculateFinalGradeWhenDbIsEmpty() throws Exception {
        //arange
        GradeConfigurationDAO gradeConfigurationDAO = Mockito.mock(GradeConfigurationDAO.class);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        when(gradeConfigurationDAO.getGradeConfiguration()).thenReturn(null);
        //act
        List<GradeValue> gradeValues = new ArrayList<>();
        gradeValues.add(new GradeValue("first", 30));
        gradeValues.add(new GradeValue("second", 30));

        EvaluateGrade evaluateGrade = new EvaluateGrade(gradeConfigurationDAO);
        //assert
        assertEquals(evaluateGrade.calculateFinalGrade(gradeValues, response).getName(), (new GradeValue("Final Grade", 0)).getName());
    }

    @Test
    public void testCalculateFinalGrafeWhenValueNameIncorectReturnBadRequest() throws Exception, InvalidGradePropertiesArray {

        GradeConfigurationDAO gradeConfigurationDAO = Mockito.mock(GradeConfigurationDAO.class);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

        GradeConfiguration gradeConfiguration = new GradeConfiguration();
        GradeProperty FIRST = new GradeProperty("first", false, 0, 30);
        GradeProperty SECOND = new GradeProperty("second", true, 50, 30);
        GradeProperty THIRD = new GradeProperty("third", false, 50, 30);
        gradeConfiguration.addGradeProperty(FIRST);
        gradeConfiguration.addGradeProperty(SECOND);
        gradeConfiguration.addGradeProperty(THIRD);

        when(gradeConfigurationDAO.getGradeConfiguration()).thenReturn(gradeConfiguration);
        List<GradeValue> gradeValues = new ArrayList();
        gradeValues.add(new GradeValue("firstA", 30)); //bad GradeValueName
        gradeValues.add(new GradeValue("second", 30));

        EvaluateGrade evaluateGrade = new EvaluateGrade(gradeConfigurationDAO);
        evaluateGrade.calculateFinalGrade(gradeValues, response);
        verify(response,times(1)).setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }

    @Test
    public void testCalculateFinalGrafeWhenValuesOver100ReturnBadRequest() throws Exception, InvalidGradePropertiesArray {

        GradeConfigurationDAO gradeConfigurationDAO = Mockito.mock(GradeConfigurationDAO.class);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);

        GradeConfiguration gradeConfiguration = new GradeConfiguration();
        GradeProperty FIRST = new GradeProperty("first", false, 0, 30);
        GradeProperty SECOND = new GradeProperty("second", true, 50, 30);
        GradeProperty THIRD = new GradeProperty("third", false, 50, 30);
        gradeConfiguration.addGradeProperty(FIRST);
        gradeConfiguration.addGradeProperty(SECOND);
        gradeConfiguration.addGradeProperty(THIRD);

        when(gradeConfigurationDAO.getGradeConfiguration()).thenReturn(gradeConfiguration);
        List<GradeValue> gradeValues = new ArrayList();
        gradeValues.add(new GradeValue("first", 400)); //values over 100
        gradeValues.add(new GradeValue("second", 800));

        EvaluateGrade evaluateGrade = new EvaluateGrade(gradeConfigurationDAO);
        evaluateGrade.calculateFinalGrade(gradeValues, response);
        verify(response,times(1)).setStatus(HttpServletResponse.SC_BAD_REQUEST);
    }


    @Test
    public void testCalculateFinalGrafeHappyPath() throws Exception, InvalidGradePropertiesArray {
        GradeConfigurationDAO gradeConfigurationDAO = Mockito.mock(GradeConfigurationDAO.class);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        GradeConfiguration gradeConfiguration = setupGradeConfiguration();
        when(gradeConfigurationDAO.getGradeConfiguration()).thenReturn(gradeConfiguration);
        List<GradeValue> gradeValues = new ArrayList();
        gradeValues.add(new GradeValue("first", 40));
        gradeValues.add(new GradeValue("second", 60));
        gradeValues.add(new GradeValue("third", 40));


        EvaluateGrade evaluateGrade = new EvaluateGrade(gradeConfigurationDAO);
        assertEquals(46,evaluateGrade.calculateFinalGrade(gradeValues, response).getValue(),0);
    }


    @Test
    public void testCalculateFinalGrafeHappyPathStudentFailed() throws Exception, InvalidGradePropertiesArray {
        GradeConfigurationDAO gradeConfigurationDAO = Mockito.mock(GradeConfigurationDAO.class);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        GradeConfiguration gradeConfiguration = setupGradeConfiguration();

        when(gradeConfigurationDAO.getGradeConfiguration()).thenReturn(gradeConfiguration);
        List<GradeValue> gradeValues = new ArrayList();
        gradeValues.add(new GradeValue("first", 40));
        gradeValues.add(new GradeValue("second", 30));
        gradeValues.add(new GradeValue("third", 40));

        EvaluateGrade evaluateGrade = new EvaluateGrade(gradeConfigurationDAO);

        assertEquals(0,evaluateGrade.calculateFinalGrade(gradeValues, response).getValue(),0);
    }

    @Test
    public void givenCompositGrades_whenCalculatingFinalGrade_calculateGrade_HappyPath() throws Exception, InvalidGradePropertiesArray {
        GradeConfigurationDAO gradeConfigurationDAO = Mockito.mock(GradeConfigurationDAO.class);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        GradeConfiguration gradeConfiguration = setupGradeConfigurationWithThirdComposite();
        when(gradeConfigurationDAO.getGradeConfiguration()).thenReturn(gradeConfiguration);
        List<GradeValue> gradeValues = new ArrayList();
        gradeValues.add(new GradeValue("first", 40));
        gradeValues.add(new GradeValue("second", 60));
        List <GradeValue> compositValues = new ArrayList<>();
        compositValues.add(new GradeValue("third_A", 100));
        compositValues.add(new GradeValue("third_B", 100));
        gradeValues.add(new GradeValue("third", compositValues));


        EvaluateGrade evaluateGrade = new EvaluateGrade(gradeConfigurationDAO);
        assertEquals(70,evaluateGrade.calculateFinalGrade(gradeValues, response).getValue(),0);
    }

    @Test
    public void givenCompositGrades_whenCalculatingFinalGradeWithFailedResults_thenFailExam() throws Exception, InvalidGradePropertiesArray {
        GradeConfigurationDAO gradeConfigurationDAO = Mockito.mock(GradeConfigurationDAO.class);
        HttpServletResponse response = Mockito.mock(HttpServletResponse.class);
        GradeConfiguration gradeConfiguration = setupGradeConfigurationWithSecondComposite();
        when(gradeConfigurationDAO.getGradeConfiguration()).thenReturn(gradeConfiguration);
        List<GradeValue> gradeValues = new ArrayList();
        gradeValues.add(new GradeValue("first", 40));
        gradeValues.add(new GradeValue("third", 60));
        List <GradeValue> compositValues = new ArrayList<>();
        compositValues.add(new GradeValue("second_A", 20));
        compositValues.add(new GradeValue("second_B", 30));
        gradeValues.add(new GradeValue("second", compositValues));


        EvaluateGrade evaluateGrade = new EvaluateGrade(gradeConfigurationDAO);
        assertEquals(0,evaluateGrade.calculateFinalGrade(gradeValues, response).getValue(),0);
    }
}