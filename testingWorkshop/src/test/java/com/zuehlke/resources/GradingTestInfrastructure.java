package com.zuehlke.resources;

import com.zuehlke.dto.grade.ExamPart;
import com.zuehlke.dto.grade.GradeConfiguration;
import com.zuehlke.dto.grade.GradeProperty;
import com.zuehlke.dto.grade.exc.InvalidGradePropertiesArray;

/**
 * Created by mimi on 3/31/2016.
 */
public class GradingTestInfrastructure {

    protected GradeConfiguration setupGradeConfiguration() throws InvalidGradePropertiesArray {
        GradeConfiguration gradeConfiguration = new GradeConfiguration();
        GradeProperty FIRST = new GradeProperty("first", false, 0, 30);
        GradeProperty SECOND = new GradeProperty("second", true, 50, 30);
        GradeProperty THIRD = new GradeProperty("third", false, 50, 40);
        gradeConfiguration.addGradeProperty(FIRST);
        gradeConfiguration.addGradeProperty(SECOND);
        gradeConfiguration.addGradeProperty(THIRD);
        return gradeConfiguration;
    }

    protected GradeConfiguration setupGradeConfigurationWithThirdComposite() throws InvalidGradePropertiesArray {
        GradeConfiguration gradeConfiguration = new GradeConfiguration();
        GradeProperty FIRST = new GradeProperty("first", false, 0, 30);
        GradeProperty SECOND = new GradeProperty("second", true, 50, 30);
        GradeProperty THIRD = new GradeProperty("third", false, 50, 40);
        THIRD.addExamPart(new ExamPart("third_A", 50,50));
        THIRD.addExamPart(new ExamPart("third_B", 0,50));
        gradeConfiguration.addGradeProperty(FIRST);
        gradeConfiguration.addGradeProperty(SECOND);
        gradeConfiguration.addGradeProperty(THIRD);
        return gradeConfiguration;
    }

    protected GradeConfiguration setupGradeConfigurationWithSecondComposite() throws InvalidGradePropertiesArray {
        GradeConfiguration gradeConfiguration = new GradeConfiguration();
        GradeProperty FIRST = new GradeProperty("first", false, 0, 30);
        GradeProperty SECOND = new GradeProperty("second", true, 50, 30);
        GradeProperty THIRD = new GradeProperty("third", false, 50, 40);
        SECOND.addExamPart(new ExamPart("second_A", 50,50));
        SECOND.addExamPart(new ExamPart("second_B", 0,50));
        gradeConfiguration.addGradeProperty(FIRST);
        gradeConfiguration.addGradeProperty(SECOND);
        gradeConfiguration.addGradeProperty(THIRD);
        return gradeConfiguration;
    }
}
