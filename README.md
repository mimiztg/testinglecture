# Testing lecture setup instructions #

### Backend: (tested part of the app)###


- Install Maven
- Install mySql server and workbench 
	- alternatively you can instal XAMPP should do the same job
	- actually anything you like, you just need to have mySql server running :)
	- set the user to root and password to admin
	- hope you get the default port 3306 :) if not set it ether to this port or switch the port in the project config file WorkshopConfiguration.yml prior to building
- with these two installed just run the build.sh script from the backend root folder to pull all the Maven dependencies and build the project.
- Run the created jar, and there you go. the app backend is running.

PS: project is by default in dev mode. All configs are located in WorkshopConfiguration.yml


### Frontend: (just for show)###

- install npm
- install bower
- with these installed run npm install (to install all dev tools)
- run bower install to install all project dependencies
- run grunt serve to get the front-end up and running (by default port 900)

PS: you might also need to install some tool to fake CORS headers when running the app locally.