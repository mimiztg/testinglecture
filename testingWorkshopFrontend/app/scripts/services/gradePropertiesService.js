'use strict';

angular.module('testingWorkshopApp')

  .factory('GradePropertiesService',
    ['$http', '$q',
      function ($http, $q) {
        var service = {};

        service.getGradeConfiguration = function (callback) {
          $http({
            method: 'GET',
            url: 'http://localhost:8080/configurations',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            }
          }).then(
            function successCallback(response) {
              if (response.status === 200 && response.data && response.data.gradeProperties) {
                callback(response.data.gradeProperties);
              }
              else {
                callback([]);
              }
            }, function errorCallback(response) {
              callback([]);
            }
          )
        };


        service.deleteGradeProperty = function (gradeProperty, success) {
          $http({
            method: 'DELETE',
            url: 'http://localhost:8080/configurations',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
            data: gradeProperty
          }).then(function(response){
            if (response.status === 200){
              success(response.data.gradeProperties);
            }
          })
        };

        service.addGradeProperty = function (gradeProperty, callback, badrequest) {
          $http({
            method: 'PUT',
            url: 'http://localhost:8080/configurations',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
            data: gradeProperty
          }).then(
            function successCallback(response) {
              if (response.status === 200 && response.data && response.data.gradeProperties) {
                callback(response.data.gradeProperties);
              }
              else {
                if (response.status === 400){
                  badrequest();
                }
              }
            }, function errorCallback(response) {
              if (response.status === 400){
                badrequest();
              }
            }
          )
        };

        return service;
      }]);
