'use strict';

angular.module('testingWorkshopApp')

  .factory('GradeEvaluatorService',
    ['$http',  'GradePropertiesService',
      function ($http, GradePropertiesService) {
        var service = {};

        service.getGradeValueArray = function (callback) {
          var gradeValueArray = [];
          GradePropertiesService.getGradeConfiguration(function (gradeProperties){
            var allProperties = gradeProperties;
            console.log(allProperties);
            allProperties.forEach(function (gradeProperty) {
              gradeValueArray.push(
                {
                  name: gradeProperty.name,
                  value: 0
                })
            });
            callback(gradeValueArray);
          });
        };


        service.evaluateGrade = function (gradeValueArray, callBackIfSuccess, badrequest) {
          $http({
            method: 'POST',
            url: 'http://localhost:8080/evaluations',
            headers: {
              'Content-Type': 'application/json',
              'Accept': 'application/json'
            },
            data: gradeValueArray
          }).then(
            function successCallback(response) {
              if (response.status === 200 && response.data) {
                callBackIfSuccess(response.data);
              }
              else {
                if (response.status === 400){
                  badrequest();
                }
              }
            }, function errorCallback(response) {
              if (response.status === 400){
                badrequest();
              }
            }
          )
        };


        return service;
      }]);
