'use strict';

angular.module('testingWorkshopApp')
  .controller('GradeEvaluationController', ['GradeEvaluatorService', '$scope', function (GradeEvaluatorService, $scope) {


    GradeEvaluatorService.getGradeValueArray(function (response){
      $scope.gradeValues = response;
    });

    $scope.gradeValuesNotPresent = function () {
      return _.isEmpty($scope.gradeValues)
    };

    $scope.evaluateGrade = function(){
      GradeEvaluatorService.evaluateGrade($scope.gradeValues, function success(grade){
        $scope.studentGrade = grade.value;
      }, function fail (){
        $scope.studentGrade = undefined;
        window.alert("Invalid action!, please check your input parameters.")
      })
    }

  }]);
