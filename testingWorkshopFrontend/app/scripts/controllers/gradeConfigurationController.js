'use strict';

angular.module('testingWorkshopApp')
  .controller('GradeConfigurationController', ['GradePropertiesService','$scope',function (GradePropertiesService, $scope) {


    GradePropertiesService.getGradeConfiguration(function (gradeProperties){
      $scope.gradeProperties = gradeProperties;
    });
    $scope.newGradeProperty = {
      property: {},  //newGradeProperty contains fields "gradePropertyId"; "name"; "compulsoryToPass"; "passPercentage"; "finalGradePercentage";

      add : function(){
        GradePropertiesService.addGradeProperty($scope.newGradeProperty.property, function(gradeProperties){
          if (!_.isEmpty(gradeProperties)){
            $scope.gradeProperties  = gradeProperties;
          }
        }, function (){
          window.alert("Invalid action!, please check your input parameters.")
        })
      }
    };

    $scope.removeProperty = function (gradeProperty){
      GradePropertiesService.deleteGradeProperty(gradeProperty, function success (properties){
        $scope.gradeProperties = properties;
      })
    }

  }]);
