'use strict';

/**
 * @ngdoc overview
 * @name testingWorkshopApp
 * @description
 * # testingWorkshopApp
 *
 * Main module of the application.
 */
angular
  .module('testingWorkshopApp', [
    'ngAnimate',
    'ngAria',
    'ngCookies',
    'ngMessages',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl',
        controllerAs: 'main'
      })
      .when('/gradeconfiguration', {
        templateUrl: 'views/gradeconfiguration.html',
        controller: 'GradeConfigurationController'
      })
      .when('/gradeevaluation', {
        templateUrl: 'views/gradeevaluation.html',
        controller: 'GradeEvaluationController'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
