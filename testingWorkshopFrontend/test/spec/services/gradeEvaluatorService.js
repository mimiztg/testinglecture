describe('Service: GradeEvaluatorService', function () {

  var httpBackend, mockedGradePropertiesService;

  // load the module
  beforeEach(module('testingWorkshopApp'));

  // mocking a service.
  beforeEach(
    module(function ($provide) {
      $provide.factory("GradePropertiesService", function () {
        return mockedGradePropertiesService = {
          getGradeConfiguration: jasmine.createSpy('getGradeConfiguration', function (callback) {
            callback([{
              "name": "prva",
              "compulsoryToPass": false,
              "passPercentage": 0.0,
              "finalGradePercentage": 2.0
            }, {
              "name": "druga",
              "compulsoryToPass": false,
              "passPercentage": 0.0,
              "finalGradePercentage": 8.0
            }, {"name": "treca", "compulsoryToPass": false, "passPercentage": 0.0, "finalGradePercentage": 12.0}]);
          })
        }
      })
    })
  );

  beforeEach(inject(function ($httpBackend, $injector) {
    httpBackend = $httpBackend;
    getService = function (name) {
      return $injector.get(name);
    };
  }));

  describe('test of service', function () {
    var gradeEvaluatorService;
    beforeEach(function () {
      gradeEvaluatorService = getService('GradeEvaluatorService');
    });
    it('should return the grade-value array in a callback', function () {
      gradeEvaluatorService.getGradeValueArray(function (gradeValueArray) {
        expect(gradeValueArray).toBe([{"name": "prva", "value": 0}, {"name": "druga", "value": 0}, {
          "name": "treca",
          "value": 0
        }]);
      });
      expect(mockedGradePropertiesService.getGradeConfiguration).toHaveBeenCalled();
      expect(mockedGradePropertiesService.getGradeConfiguration).toHaveBeenCalledTimes(1);
    });
  })
});
